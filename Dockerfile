FROM python:3.9 
COPY track_thor_update.py update-thornode-dockerfile.sh ./
RUN apt-get update && apt-get install -y git jq
RUN pip install requests discord-webhook discord.py
RUN chmod +x update-thornode-dockerfile.sh
RUN git clone https://gitlab.com/mayachain/devops/node-launcher.git
CMD ["python", "./track_thor_update.py"]