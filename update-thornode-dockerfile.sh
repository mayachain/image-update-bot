#!/bin/bash

set -e

VERSION=$1
PROJECT_ID=$2
GITLAB_API_TOKEN=$3
DOCKERFILE_PATH=${DOCKERFILE_PATH}
REPO_ID=${REPO_ID}
DOCKER_USERNAME=${DOCKER_USERNAME}
DOCKER_PASSWORD=${DOCKER_PASSWORD}

echo "Updating to version: $VERSION"

# Gitlab API request
echo "Fetching tag details from GitLab..."
TAG_DETAILS=$(curl "https://gitlab.com/api/v4/projects/$PROJECT_ID/registry/repositories/$REPO_ID/tags/mainnet-$VERSION")

# Manifet Digest Hash
MANIFEST_DIGEST=$(echo $TAG_DETAILS | jq -r '.digest')

# Change branch
cd node-launcher
git checkout daemons
git pull origin daemons

# Create FROM for Dockerfile
NEW_FROM="FROM registry.gitlab.com/thorchain/thornode:mainnet-$VERSION@$MANIFEST_DIGEST"

# Write Dockerfile
echo "Updating Dockerfile..."
sed -i "s|^FROM registry.gitlab.com/thorchain/thornode:.*|$NEW_FROM|" $DOCKERFILE_PATH

# Docker commands
echo "Logging into Docker and building image..."
docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD registry.gitlab.com/mayachain/devops/node-launcher
docker build --platform linux/amd64 -t registry.gitlab.com/mayachain/devops/node-launcher:thornode-daemon-$VERSION ./ci/images/thornode-daemon/.
echo "Pushing image to registry..."
docker push registry.gitlab.com/mayachain/devops/node-launcher:thornode-daemon-$VERSION

echo "Script finished."