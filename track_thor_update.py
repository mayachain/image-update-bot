import requests
import time
import subprocess
import logging
import os
from discord_webhook import DiscordWebhook

logging.basicConfig(level=logging.INFO)

PROJECT_ID = os.environ.get('PROJECT_ID')
DISCORD_WEBHOOK_URL = os.environ.get('DISCORD_WEBHOOK_URL')

def get_tags():
    url = f"https://gitlab.com/api/v4/projects/{PROJECT_ID}/repository/tags"
    try:
        response = requests.get(url)
        response.raise_for_status()
        tags = [tag['name'] for tag in response.json()]
        logging.info(f"Fetched tags: {tags}")
        return tags
    except requests.RequestException as e:
        logging.error(f"Error fetching tags: {e}")
        return []

def send_discord_notification(message):
    webhook = DiscordWebhook(url=DISCORD_WEBHOOK_URL, content=message)
    webhook.execute()
    logging.info(f"Notification sent to Discord: {message}")

def main():
    logging.info("Starting script...")
    last_seen_tags = get_tags()
    if last_seen_tags:
        logging.info("Last seen tag: %s", last_seen_tags[0])
    else:
        logging.warning("No tags fetched on startup. Will retry in next cycle.")

    while True:
        time.sleep(3600)  # 1 hour
        logging.info("Waking up to check for new tags...")
        current_tags = get_tags()
        logging.info(f"Current tags: {current_tags}")

        new_tags = set(current_tags) - set(last_seen_tags)
        logging.info(f"New tags found: {new_tags}")

        if new_tags:
            new_version = next(iter(new_tags))
            message = "Found new thor version with tag -> " + new_version
            send_discord_notification(message)

            version = new_version.replace('v', '') 
            completed = subprocess.run(["./update-thornode-dockerfile.sh", version, PROJECT_ID])
            logging.info(f"update-thornode-dockerfile.sh with version -> {version}")

            if completed.returncode == 0:
                success_message = "Thor image updated to " + new_version
                send_discord_notification(success_message)
            else:
                error_message = "Fail to update thor image"
                send_discord_notification(error_message)
                logging.error(error_message)

        last_seen_tags = current_tags

if __name__ == "__main__":
    main()
