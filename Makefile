.PHONY: all build run stop remove

all: build run

stop:
	-docker stop my-thor-updater

remove: stop
	-docker rm my-thor-updater

build: remove
	docker build -t update-thornode .

run: 
	docker run -d --log-opt max-size=10m --log-opt max-file=7 --name my-thor-updater update-thornode